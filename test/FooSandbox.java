/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
import fr.devinsy.kiss4web.ServletDispatcher;

/**
 * Kiss4Web tests.
 */
class FooSandbox
{
	static private org.apache.log4j.Logger logger;

	static
	{
		// Initialize logger.
		org.apache.log4j.Logger logger = null;

		org.apache.log4j.BasicConfigurator.configure();

		logger = org.apache.log4j.Logger.getRootLogger();
		// logger.setLevel (org.apache.log4j.Level.INFO);
		logger.setLevel(org.apache.log4j.Level.INFO);

		logger.info("Enter");

		//
		logger.info("Set the log file format...");

		// log =
		// org.apache.log4j.Category.getInstance(Application.class.getName());
		logger.info("... done.");

		logger.debug("Exit");
		logger = org.apache.log4j.Logger.getLogger(FooSandbox.class.getName());
	}

	/**
	 *
	 */
	public static String check(final String title, final StringBuffer source, final String model)
	{
		String result;

		if (source.indexOf(model) == -1)
		{
			result = String.format("%-40s  ->  KO  <-", title) + "\nGet:\n" + source + "\nWaiting:\n" + model;

		}
		else
		{
			result = String.format("%-40s    [ OK ]  ", title);
		}

		//
		return (result);
	}

	/**
	 *
	 */
	public static void main(final String[] args)
	{
		System.out.println("----------------------------");
		System.out.println(testCaller("/", "fr.devinsy.website"));
		System.out.println(testCaller("/good/", "fr.devinsy.website"));
		System.out.println(testCaller("/good/morning", "fr.devinsy.website"));
		System.out.println(testCaller("/good/day_day", "fr.devinsy.website"));
		System.out.println(testCaller("/good/day.xhtml", "fr.devinsy.website"));
	}

	/**
	 * 
	 */
	public static String testCaller(final String pathInfo, final String prefix)
	{
		String result;

		result = "[" + pathInfo + "]=>[" + ServletDispatcher.pathInfoToClassName(pathInfo, prefix) + "]";

		//
		return (result);
	}
}
