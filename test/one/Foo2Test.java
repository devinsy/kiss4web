/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package one;

import org.junit.Test;

public class Foo2Test
{
	// private Logger logger =
	// LoggerFactory.getLogger(PdfGenerationAmqpServiceInjectedTest.class);

	/**
     * 
     */
	@Test
	public void test2a()
	{
		// logger.debug("===== test starting...");

		// logger.debug("===== test done.");
	}

}
