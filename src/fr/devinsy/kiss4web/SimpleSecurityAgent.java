/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.StringList;

/**
 *
 */
public class SimpleSecurityAgent
{
	static private final Logger logger = LoggerFactory.getLogger(SimpleSecurityAgent.class);
	public static final String USERID_LABEL = "securityAgent.userId";
	public static final String ACCOUNTID_LABEL = "securityAgent.accountId";
	public static final String AUTH_LABEL = "securityAgent.auth";
	private String userIdLabel;
	private String accountIdLabel;
	private String authLabel;
	private String secretKey;
	private static final int DEFAULT_AUTHENTICATION_DURATION = 60 * 60;
	private int authenticationDuration;

	/**
	 * 
	 */
	public SimpleSecurityAgent(final String prefix, final String secretKey)
	{
		this.userIdLabel = prefix + "." + USERID_LABEL;
		this.accountIdLabel = prefix + "." + ACCOUNTID_LABEL;
		this.authLabel = prefix + "." + AUTH_LABEL;
		this.secretKey = secretKey;
		this.authenticationDuration = DEFAULT_AUTHENTICATION_DURATION;
	}

	/**
	 *
	 */
	public String accountId(final HttpServletRequest request)
	{
		String result;

		result = (String) CookieHelper.getCookieValue(request, this.accountIdLabel);

		//
		return (result);
	}

	/**
	 *
	 */
	public String auth(final HttpServletRequest request)
	{
		String result;

		result = (String) CookieHelper.getCookieValue(request, this.authLabel);

		//
		return (result);
	}

	/**
	 * This method builds a key from keys and a secret key.
	 */
	public String computeAuth(final String... keys)
	{
		String result;

		if (keys == null)
		{
			result = null;
		}
		else
		{
			// Add a secret key to the key list.
			String[] targetKeys = new String[keys.length + 1];
			for (int keyIndex = 0; keyIndex < keys.length; keyIndex++)
			{
				targetKeys[keyIndex] = keys[keyIndex];
			}
			targetKeys[keys.length] = this.secretKey;

			//
			result = digest(targetKeys);
		}

		//
		return (result);
	}

	/**
	 * 
	 * @return
	 */
	public int getAuthenticationDuration()
	{
		return authenticationDuration;
	}

	/**
	 * Check authentication and refresh it (reset countdown).
	 */
	public boolean isAuthenticated(final HttpServletRequest request, final HttpServletResponse response)
	{
		boolean result;

		String accountId = accountId(request);
		String userId = userId(request);
		String auth = auth(request);
		logger.info("cook=[" + auth + "]");

		if (auth == null)
		{
			result = false;
		}
		else if (auth.equals(computeAuth(accountId, userId, request.getRemoteAddr())))
		{
			result = true;

			// Refresh cookies.
			setAuthenticated(request, response, accountId, userId);
		}
		else
		{
			result = false;
		}

		//
		return (result);
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public String md5sumWithSecret(final String source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			String key = source + this.secretKey;
			result = digest(key);
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void reset(final HttpServletRequest request, final HttpServletResponse response)
	{
		CookieHelper.reset(response, this.authLabel);
		CookieHelper.reset(response, this.accountIdLabel);
		CookieHelper.reset(response, this.userIdLabel);
	}

	/**
	 *
	 */
	public void setAuthenticated(final HttpServletRequest request, final HttpServletResponse response, final String accountId, final String userId)
	{
		// Refresh cookie.
		String auth = computeAuth(String.valueOf(accountId), userId, request.getRemoteAddr());

		response.addCookie(CookieHelper.buildCookie(this.authLabel, auth, this.authenticationDuration));
		response.addCookie(CookieHelper.buildCookie(this.accountIdLabel, accountId, this.authenticationDuration));
		response.addCookie(CookieHelper.buildCookie(this.userIdLabel, userId, this.authenticationDuration));

		logger.info("set [" + auth + "," + accountId + "," + userId + "," + request.getRemoteAddr() + ")");
	}

	/**
	 * 
	 * @param duration
	 */
	public void setAuthenticationDuration(final int duration)
	{
		this.authenticationDuration = duration;
	}

	/**
	 *
	 */
	public String userId(final HttpServletRequest request)
	{
		String result;

		result = (String) CookieHelper.getCookieValue(request, this.userIdLabel);

		//
		return (result);
	}

	/**
	 * 
	 */
	static public String digest(final String... keys)
	{
		String result;

		if (keys == null)
		{
			result = null;
		}
		else
		{
			//
			StringList targetKey = new StringList();
			for (String key : keys)
			{
				targetKey.append(key);
			}

			//
			result = DigestUtils.sha256Hex(targetKey.toString());
		}

		//
		return (result);
	}
}