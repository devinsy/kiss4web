/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web.security;

import java.util.Iterator;
import java.util.Vector;

/**
 *
 */
public class Groups extends Vector<Group>
{
	private static final long serialVersionUID = 6238581648850758903L;

	/**
	 *
	 */
	public Groups()
	{
		super();
	}

	/*
	 *
	 */
	public boolean contains(final String name)
	{
		boolean result;

		if (get(name) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return (result);
	}

	/**
	 *
	 */
	public Group get(final String name)
	{
		Group result;

		if (name == null)
		{
			result = null;
		}
		else
		{
			result = null;
			boolean ended = false;
			Iterator<Group> iterator = this.iterator();
			while (!ended)
			{
				if (iterator.hasNext())
				{
					Group group = iterator.next();
					if (group.name().equals(name))
					{
						ended = true;
						result = group;
					}
				}
				else
				{
					ended = true;
					result = null;
				}
			}
		}

		//
		return (result);
	}

	/**
	 *
	 */
	public Vector<String> getLoginGroups(final String login)
	{
		Vector<String> result;

		result = new Vector<String>();
		Iterator<Group> iterator = this.iterator();

		while (iterator.hasNext())
		{
			Group group = iterator.next();

			if (group.members().contains(login))
			{
				result.add(group.name());
			}
		}

		//
		return (result);
	}

	/**
	 * 
	 */
	public String getLoginGroupsString(final String login)
	{
		String result;

		Vector<String> groups = getLoginGroups(login);

		StringBuffer string = new StringBuffer();

		for (String group : groups)
		{
			if (string.length() == 0)
			{
				string.append(group);
			}
			else
			{
				string.append(",");
				string.append(group);
			}
		}

		result = string.toString();

		//
		return (result);
	}

	/**
	 *
	 */
	@Override
	public String toString()
	{
		String result;

		StringBuffer out;
		out = new StringBuffer();

		Iterator<Group> iterator = this.iterator();

		while (iterator.hasNext())
		{
			out.append(iterator.next().toString() + "\n");
		}

		result = out.toString();

		//
		return (result);
	}
}

// ////////////////////////////////////////////////////////////////////////
