package fr.devinsy.kiss4web.security;

import java.util.Iterator;

/**
 *
 */
public class SecurityAgent
{
	protected Users users;
	protected Groups groups;
	protected Groups permissions;

	/**
	 *
	 */
	public SecurityAgent(final String path) throws Exception
	{
		this.users = UsersFileReader.load(path + "users.conf");
		this.groups = GroupsFileReader.load(path + "groups.conf");
		this.permissions = GroupsFileReader.load(path + "permissions.conf");
	}

	/**
	 *
	 */
	public boolean authenticate(final String login, final String password)
	{
		boolean result;

		User user = this.users.getByLogin(login);

		if (user == null)
		{
			result = false;
		}
		else if (user.password().equals(password))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return (result);
	}

	/**
	 *
	 */
	public boolean checkPermission(final String url, final String login)
	{
		boolean result = false;

		Group permitGroups = this.permissions.get(url);

		if (permitGroups == null)
		{
			result = false;
		}
		else
		{
			//
			boolean ended = false;
			Iterator<String> iterator = permitGroups.members().iterator();
			while (!ended)
			{
				if (!iterator.hasNext())
				{
					ended = true;
					result = false;
				}
				else
				{
					String groupName = iterator.next();
					if (groupName.equals("*"))
					{
						result = true;
						ended = true;
					}
					else
					{
						Group members = this.groups.get(groupName);

						if (members == null)
						{
							result = false;
						}
						else
						{
							if (members.contains(login))
							{
								ended = true;
								result = true;
							}
						}
					}
				}
			}
		}

		//
		return (result);
	}

	/**
	 *
	 */
	public Groups groups()
	{
		Groups result;

		result = this.groups;

		//
		return (result);
	}

	/**
	 *
	 */
	public Groups permissions()
	{
		Groups result;

		result = this.permissions;

		//
		return (result);
	}

	/**
	 *
	 */
	public Users users()
	{
		Users result;

		result = this.users;

		//
		return (result);
	}
}
