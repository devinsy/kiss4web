/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web.security;

import java.util.Vector;

/**
 *
 */
public class Group
{
	protected String name;
	protected Vector<String> members;

	/**
	 *
	 */
	public Group()
	{
		this.name = null;
		this.members = new Vector<String>();
	}

	/**
	 *
	 */
	public void addMember(final String login)
	{
		if ((login != null) && (login.length() != 0))
		{
			this.members.add(login);
		}
	}

	/**
	 *
	 */
	public boolean contains(final String name)
	{
		boolean result = false;

		result = this.members.contains(name);

		//
		return (result);
	}

	/**
	 *
	 */
	public Vector<String> members()
	{
		Vector<String> result;

		result = this.members;

		//
		return (result);
	}

	/**
	 *
	 */
	public String name()
	{
		String result;

		result = this.name;

		//
		return (result);
	}

	/**
	 *
	 */
	public Group setName(final String name)
	{
		this.name = name;

		//
		return (this);
	}

	/**
	 *
	 */
	@Override
	public String toString()
	{
		String result;

		result = "|" + this.name + "|" + this.members + "|";

		//
		return (result);
	}
}

// ////////////////////////////////////////////////////////////////////////
