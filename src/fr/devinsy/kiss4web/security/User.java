/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web.security;

/**
 *
 */
public class User
{
	protected String login;
	protected String password;
	protected String realName;
	protected String email;

	/**
	 *
	 */
	public User()
	{
		this.login = null;
		this.password = null;
		this.realName = null;
		this.email = null;
	}

	/**
	 *
	 */
	public String email()
	{
		String result;

		result = this.email;

		//
		return (result);
	}

	/**
	 *
	 */
	public String login()
	{
		String result;

		result = this.login;

		//
		return (result);
	}

	/**
	 *
	 */
	public String password()
	{
		String result;

		result = this.password;

		//
		return (result);
	}

	/**
	 *
	 */
	public String realName()
	{
		String result;

		result = this.realName;

		//
		return (result);
	}

	/**
	 *
	 */
	public User setEmail(final String email)
	{
		this.email = email;

		//
		return (this);
	}

	/**
	 *
	 */
	public User setLogin(final String login)
	{
		this.login = login;

		//
		return (this);
	}

	/**
	 *
	 */
	public User setPassword(final String password)
	{
		this.password = password;

		//
		return (this);
	}

	/**
	 *
	 */
	public User setRealName(final String realName)
	{
		this.realName = realName;

		//
		return (this);
	}

	/**
	 *
	 */
	@Override
	public String toString()
	{
		String result;

		result = "|" + this.login + "|" + this.password + "|" + this.realName + "|";

		//
		return (result);
	}
}
