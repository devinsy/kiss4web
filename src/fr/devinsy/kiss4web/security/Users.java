/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web.security;

import java.util.Iterator;
import java.util.Vector;

/**
 *
 */
public class Users extends Vector<User>
{
	private static final long serialVersionUID = 6140538630004281217L;

	/**
	 *
	 */
	public Users()
	{
		super();
	}

	/*
	 *
	 */
	public boolean contains(final String login)
	{
		boolean result;

		if (getByLogin(login) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return (result);
	}

	/**
	 *
	 */
	public User getByLogin(final String login)
	{
		User result;

		if (login == null)
		{
			result = null;
		}
		else
		{
			result = null;
			boolean ended = false;
			Iterator<User> iterator = this.iterator();
			while (!ended)
			{
				if (iterator.hasNext())
				{
					User user = iterator.next();
					if (user.login().equals(login))
					{
						ended = true;
						result = user;
					}
				}
				else
				{
					ended = true;
					result = null;
				}
			}
		}

		//
		return (result);
	}

	/**
	 *
	 */
	@Override
	public String toString()
	{
		String result;

		StringBuffer out;
		out = new StringBuffer();

		Iterator<User> iterator = this.iterator();

		while (iterator.hasNext())
		{
			out.append(iterator.next().toString() + "\n");
		}

		result = out.toString();

		//
		return (result);
	}
}

// ////////////////////////////////////////////////////////////////////////
