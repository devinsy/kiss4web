/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web.security;

import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Properties;

/**
 *
 */
public class GroupsFileReader
{
	/**
	 *
	 */
	static public Groups load(final String fileName) throws Exception
	{
		Groups result;

		result = new Groups();

		Properties properties = new Properties();
		try
		{
			properties.load(new FileInputStream(fileName));

			Iterator<Object> iterator = properties.keySet().iterator();
			while (iterator.hasNext())
			{
				String key = (String) iterator.next();
				String valueLine = (String) properties.get(key);

				//
				String[] values = valueLine.split(",");

				Group group = new Group();
				group.setName(key);
				for (String value : values)
				{
					group.addMember(value.trim());
				}

				result.add(group);
			}
		}
		catch (Exception exception)
		{
			throw new Exception("can't load (" + fileName + ")", exception);
		}

		//
		return (result);
	}
}

// ////////////////////////////////////////////////////////////////////////
