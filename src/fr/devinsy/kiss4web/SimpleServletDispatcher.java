/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.StringList;

/**
 * Rename KissDispatcher?
 */
public class SimpleServletDispatcher extends HttpServlet
{
	public enum ContentDispositionType
	{
		ATTACHMENT, INLINE
	}

	private static final long serialVersionUID = -3471226305721330069L;

	static private Logger logger = LoggerFactory.getLogger(SimpleServletDispatcher.class);

	static final private Pattern SHORT_REWRITED_URL_CLASS = Pattern.compile("^([^-]+)-.+\\.xhtml$");
	static final private Pattern SHORT_REWRITED_URL_PARAMETERS = Pattern.compile("^[^-]+-(.+)\\.xhtml$");
	static final private Pattern LONG_REWRITED_URL_CLASS = Pattern.compile("^([^-]+)-/.*$");
	// static final protected Pattern LONG_REWRITED_URL_PARAMETERS =
	// Pattern.compile("^.+-/(.)+*$");
	static final private Pattern REWRITE_PARAMETER = Pattern.compile("[^%\\w\\d]");
	static final public int CACHE_AGE = 2 * 60 * 60;

	private String webclassesRootPath;

	/*
	 * "Code can be shortest, speedest and memory smallest, but not the three in same time, only two", unknow citation.
	 * 
	 * Note: characters array avalaible here
	 * http://fr.wikipedia.org/wiki/Table_des_caract%C3%A8res_Unicode_%280000-0FFF%29
	 */
	static protected char NONE = (char) 0;

	static protected int[] rewritingParameterMapping = {
	/* 00 */NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
	/* 10 */NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
	/* 20 */'-', NONE, NONE, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-',
	/* 30 */'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '-', '-', '-', '-', '-',
	/* 40 */'\u0040', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
	/* 50 */'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '-', '-', '-', '-', '-',
	/* 60 */'-', '\u0061', '\u0062', '\u0063', '\u0064', '\u0065', '\u0066', '\u0067', '\u0068', '\u0069', '\u006A', '\u006B', '\u006C', '\u006D', '\u006E', '\u006F',
	/* 70 */'\u0070', '\u0071', '\u0072', '\u0073', '\u0074', '\u0075', '\u0076', '\u0077', '\u0078', '\u0079', '\u007A', '\u007B', '\u007C', '\u007D', '-', '-',
	/* 80 */NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
	/* 90 */NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE, NONE,
	/* A0 */'\u00A0', '\u00A1', '\u00A2', '\u00A3', '\u00A4', '\u00A5', '\u00A6', '\u00A7', '\u00A8', '\u00A9', '\u00AA', '\u00AB', '\u00AC', '\u00AD', '\u00AE', '\u00AF',
	/* B0 */'-', '\u00B1', '\u00B2', '\u00B3', '\u00B4', '\u00B5', '\u00B6', '\u00B7', '\u00B8', '\u00B9', '\u00BA', '\u00BB', '\u00BC', '\u00BD', '\u00BE', '\u00BF',
	/* C0 */'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i',
	/* D0 */'\u00D0', '\u00D1', 'o', 'o', 'o', 'o', 'o', 'o', '\u00D8', 'u', 'u', 'u', 'u', 'y', '\u00DE', '\u00DF',
	/* E0 */'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i',
	/* F0 */'o', 'n', 'o', 'o', 'o', 'o', 'o', '\u00F7', '-', 'u', 'u', 'u', 'u', 'y', '-', 'y' };

	/**
	 * 
	 *
	 */
	public void dispatch(final HttpServletRequest request, final HttpServletResponse response, final String webClassesRootPath) throws IOException, ServletException
	{
		long startTime = new Date().getTime();

		logger.info("==================================================");
		logger.info("getContextPath=[" + request.getContextPath() + "]");
		logger.info("getPathInfo=[" + request.getPathInfo() + "]");
		logger.info("getPathTranslated=[" + request.getPathTranslated() + "]");
		logger.info("getQueryString=[" + request.getQueryString() + "]");
		logger.info("getRequestURI=[" + request.getRequestURI() + "]");
		logger.info("getRequestURL=[" + request.getRequestURL() + "]");
		logger.info("getServletPath=[" + request.getServletPath() + "]");

		//
		/*
		 * In past, possibility to use the servlet path was enable. It is too
		 * complexe, not kiss mind.
		 * 
		 * String path;
		 * if (request.getPathInfo() == null)
		 * {
		 * 		// web.xml url-pattern= *.xhtml
		 * 		path = request.getServletPath();
		 * }
		 * else
		 * {
		 * 		// web.xml url-pattern =
		 * 		path = request.getPathInfo();
		 * }
		 * 
		 * https://issues.apache.org/bugzilla/show_bug.cgi?id=11318
		 * A number of features have been introduced in later versions of Tomcat
		 * that relate to URI encoding. This includes a URIEncoding attribute
		 * on the Coyote HTTP/1.1 connector which defaults to ISO-8859-1.
		 *
		 * According to the servlet specification:
		 * HttpServletRequest.getPathInfo() should be decoded by the web container;
		 * HttpServletRequest.getRequestURI() should not be decoded by the web container.
		 * 
		 * 
		 * http://stackoverflow.com/questions/15278512/safe-html-form-accept-charset/15587140#15587140
		 * The standard URL encoding must use UTF-8 yet servlets not only default to ISO-8859-1 but don't offer
		 * any way to change that with code.
		 * Sure you can req.setRequestEncoding("UTF-8") before you read anything, but for some ungodly reason this only affects request body,
		 * not query string parameters. There is nothing in the servlet request interface to specify the encoding used for query string parameters.
		 * Using ISO-8859-1 in your form is a hack. Using this ancient encoding will cause more problems than solve for sure.
		 * Especially since browsers do not support ISO-8859-1 and always treat it as Windows-1252. Whereas servlets treat ISO-8859-1 as ISO-8859-1,
		 * so you will be screwed beyond belief if you go with this.
		 * To change this in Tomcat for example, you can use the URIEncoding attribute in your <connector> element:
		 * <connector ... URIEncoding="UTF-8" ... />
		 * 
		 * If you don't use a container that has these settings, can't change its settings or some other issue,
		 * you can still make it work because ISO-8859-1 decoding retains full information from the original binary.
		 * String correct = new String(request.getParameter("test").getBytes("ISO-8859-1"), "UTF-8")
		 * 
		 * 
		 * http://tomcat.apache.org/tomcat-5.5-doc/config/http.html
		 * URIEncoding	
		 * This specifies the character encoding used to decode the URI bytes, after %xx decoding the URL.
		 * If not specified, ISO-8859-1 will be used. 
		 * 
		 */
		// String path = request.getRequestURI();
		String path = request.getPathInfo();

		if (path == null)
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			out.println("<html><head></head><body>");
			out.println("Unknow path.");
			out.println("</body></html>");

			out.close();
		}
		else if ((!path.endsWith("/")) && (!path.endsWith(".xhtml")) && (!path.contains("-/")))
		{
			// path = getServletContext().getRealPath("/") +
			// request.getRequestURI();
			// First, search file in the WebContent root.
			path = getServletContext().getRealPath("/") + request.getPathInfo();
			logger.debug("path1=" + path);
			if (!new File(path).exists())
			{
				// If file is not in WebContent root, search it in packaged
				// classes.
				path = getServletContext().getRealPath("/") + "WEB-INF/classes/" + webClassesRootPath.replaceAll("\\.", "/") + request.getPathInfo();
			}
			logger.debug("path2=" + path);

			returnInlineFile(response, new File(path), getServletContext().getMimeType(path));
			logger.info("File returned directly [" + path + "] with mimetype [" + getServletContext().getMimeType(path) + "].");
		}
		else
		{
			String className = pathInfoToClassName(path, webClassesRootPath);
			logger.info("className=[" + className + "]");

			HttpServlet servlet = instanciateServlet(className);

			// servlet.getServletContext().setAttribute(arg0, arg1);

			if (servlet == null)
			{
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();

				out.println("<html><head></head><body>");
				out.println("Unknow page.");
				out.println("</body></html>");

				out.close();
			}
			else if (isAuthorized(request, response))
			{
				servlet.init(this.getServletConfig());
				servlet.service(request, response);
			}
			else
			{
				/*
				 * TODO
				 *
				 * response.setContentType ("text/html"); PrintWriter
				 * out = response.getWriter();
				 * 
				 * out.println ("<html><head></head><body>"); out.println
				 * ("Not authorized page."); out.println ("</body></html>");
				 */
			}
		}

		//
		long endTime = new Date().getTime();
		logger.debug("TIME: {}ms {}", endTime - startTime, request.getPathInfo());
	}

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		dispatch(request, response, this.webclassesRootPath);
	}

	/**
	 * Backward compatibility.
	 */
	public void doIt(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		dispatch(request, response, this.webclassesRootPath);
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		dispatch(request, response, this.webclassesRootPath);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
		super.init();
		this.webclassesRootPath = getInitParameter("webClassesRootPath");

		// Set logger.
		String logFilepathname = getInitParameter("log4j-init-file");
		if (logFilepathname != null)
		{
			try
			{
				System.out.println("Log configuration found (" + logFilepathname + "), will use it.");
				org.apache.log4j.PropertyConfigurator.configure(getServletContext().getRealPath("/") + logFilepathname);
			}
			catch (Exception exception)
			{
				System.out.println("Log configuration FILE NOT FOUND (" + logFilepathname + "), use of the basic configurator.");
				org.apache.log4j.BasicConfigurator.configure();
			}

			logger = LoggerFactory.getLogger(this.getClass());
			logger.info("Log initialization done.");
		}
	}

	/**
	 * 
	 */
	public boolean isAuthorized(final HttpServletRequest request, final HttpServletResponse response)
	{
		boolean result;

		result = true;

		//
		return (result);
	}

	/**
	 *
	 */
	static public HttpServlet instanciateServlet(final String className)
	{
		HttpServlet result;

		Class<HttpServlet> servletClass = null;
		try
		{
			servletClass = (Class<HttpServlet>) Class.forName(className);
		}
		catch (java.lang.ClassNotFoundException exception)
		{
			result = null;
		}

		logger.info("class=" + servletClass);

		if (servletClass == null)
		{
			result = null;
			logger.error("Unknow page: (" + className + ")");
		}
		else
		{
			try
			{
				result = servletClass.newInstance();
			}
			catch (java.lang.InstantiationException exception)
			{
				logger.error("Can't instanciate servlet (" + className + ")");
				result = null;
			}
			catch (java.lang.IllegalAccessException exception)
			{
				logger.error("(2) Can't instanciate servlet (" + className + ")");
				result = null;
			}
		}

		//
		return (result);
	}

	/**
	 * Extract values from a path.
	 * 
	 * Example:
	 * 
	 * <pre>
	 * "/article-/123/doors/open.xhtml";
	 * => "123", "doors" and "open".
	 * </pre>
	 */
	static public String[] longRewritedUrlParameters(final String path)
	{
		String[] result;

		result = path.substring(path.indexOf("-/") + 2).split("/");

		//
		return (result);
	}

	/**
	 * Convert a path in a class name, using easy conventions.
	 * 
	 * <pre>
	 * "/" => "Index_xhtml"
	 * "/good/" => "good.Good_xhtml"
	 * "/good/morning.xhtml" => "good.Morning_xhtml"
	 * "/good/morning_girl.xhtml" => "good.Morning_girl_xhtml"
	 * "/good/morning-123.xhtml" => "good.Morning_xhtml" ('123' is detected as a parameter, it will be
	 * decoded in the class called later).
	 * "/good/morning-/12/toto.jpg" => "good.Morning" ('12' and 'toto.jpg" are detected as a parameter, they
	 * will be decoded in the class called later).
	 * </pre>
	 * 
	 */
	static public String pathInfoToClassName(final String pathInfo)
	{
		String result;

		if ((pathInfo == null) || (pathInfo.length() == 0))
		{
			result = null;
		}
		else
		{
			if (pathInfo.equals("/"))
			{
				result = "Index_xhtml";
			}
			else
			{
				int keywordIndex = pathInfo.lastIndexOf("-/");

				if (keywordIndex != -1)
				{
					// Long rewrited URL case.
					String[] tokens = pathInfo.substring(0, keywordIndex).split("/");

					StringList name = new StringList();
					// Note: as pathInfo starts always with a '/', the first
					// good token index is 1.
					for (int tokenCounter = 1; tokenCounter < (tokens.length - 1); tokenCounter++)
					{
						name.append(tokens[tokenCounter]);
						name.append('.');
					}

					String lastToken = tokens[tokens.length - 1];
					name.append(lastToken.substring(0, 1).toUpperCase()).append(lastToken.substring(1).replace('.', '_'));
					result = name.toString();
				}
				else
				{
					String[] tokens = pathInfo.split("/");

					StringList name = new StringList();
					// Note: as pathInfo starts always with a '/', the first
					// good token index is 1.
					for (int tokenCounter = 1; tokenCounter < (tokens.length - 1); tokenCounter++)
					{
						name.append(tokens[tokenCounter]);
						name.append('.');
					}

					String lastToken = tokens[tokens.length - 1];
					if (pathInfo.endsWith("/"))
					{
						name.append(lastToken).append(".").append(lastToken.substring(0, 1).toUpperCase()).append(lastToken.substring(1)).append("_xhtml");
					}
					else
					{
						Matcher matcher = SHORT_REWRITED_URL_CLASS.matcher(lastToken);
						if (matcher.matches())
						{
							// Short rewrited URL case.
							// logger.debug("group 1=[" + matcher.group(1) +
							// "]");
							lastToken = matcher.group(1) + ".xhtml";
						}

						name.append(lastToken.substring(0, 1).toUpperCase()).append(lastToken.substring(1).replace('.', '_'));
					}

					result = name.toString();
					logger.debug("==>[" + tokens[tokens.length - 1] + "]");
				}
			}
		}

		logger.info("[" + pathInfo + "] => [" + result + "]");

		//
		return (result);
	}

	/**
	 * 
	 */
	static public String pathInfoToClassName(final String pathInfo, final String prefix)
	{
		String result;

		String className = pathInfoToClassName(pathInfo);

		if (prefix == null)
		{
			result = className;
		}
		else if (prefix.endsWith("."))
		{
			result = prefix + className;
		}
		else
		{
			result = prefix + "." + className;
		}

		//
		return (result);
	}

	/**
	 * 
	 */
	static public void returnAttachmentFile(final HttpServletResponse response, final File file, final String mimeType) throws IOException
	{
		returnFile(response, file, mimeType, ContentDispositionType.ATTACHMENT);
	}

	/**
	 * 
	 */
	static public void returnFile(final HttpServletResponse response, final File file, final String mimeType, final ContentDispositionType contentDisposition) throws IOException
	{

		if ((file == null) || (!file.exists()))
		{
			logger.warn("File not found [" + file.getAbsolutePath() + "]");
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		else
		{
			/*
			 * response.setContentType("application/" + extension);
			 * response.setContentLength((int) data.length);
			 * response.setHeader("Content-Disposition"
			 * ,"attachment; filename=\"" + filename + "\"");
			 * response.flushBuffer();
			 */
			response.reset();
			response.setContentType(mimeType);
			response.setContentLength((int) file.length());
			String contentDispositionToken;
			if (contentDisposition == ContentDispositionType.ATTACHMENT)
			{
				contentDispositionToken = "attachment";
			}
			else
			{
				contentDispositionToken = "inline";
			}
			response.setHeader("Content-Disposition", contentDispositionToken + "; filename=\"" + file.getName() + "\"");

			response.setDateHeader("Expires", new Date().getTime() + CACHE_AGE * 1000);
			response.setHeader("Cache-Control", "max-age=" + CACHE_AGE);

			response.flushBuffer();

			ServletOutputStream out = response.getOutputStream();

			FileInputStream in = null;
			try
			// Only for the in.
			{
				byte[] buffer = new byte[64 * 1024];

				in = new FileInputStream(file);
				boolean ended = false;
				while (!ended)
				{
					int count = in.read(buffer);

					if (count == -1)
					{
						ended = true;
					}
					else
					{
						out.write(buffer, 0, count);
					}
				}
				out.flush();
				out.close();
			}
			catch (IOException exception)
			{
				response.sendError(HttpServletResponse.SC_PARTIAL_CONTENT);
			}
			finally
			{
				if (in != null)
				{
					in.close();
				}
			}
		}
	}

	/**
	 * 
	 */
	static public void returnFile(final HttpServletResponse response, final String fileName, final byte[] data, final String mimeType, final ContentDispositionType contentDisposition,
			final int cacheAge) throws IOException
	{

		if (data == null)
		{
			logger.warn("data is null.");
		}

		/*
		 * response.setContentType("application/" + extension);
		 * response.setContentLength((int) data.length);
		 * response.setHeader("Content-Disposition"
		 * ,"attachment; filename=\"" + filename + "\"");
		 * response.flushBuffer();
		 */
		response.reset();
		response.setContentType(mimeType);
		response.setContentLength(data.length);
		String contentDispositionToken;
		if (contentDisposition == ContentDispositionType.ATTACHMENT)
		{
			contentDispositionToken = "attachment";
		}
		else
		{
			contentDispositionToken = "inline";
		}
		response.setHeader("Content-Disposition", contentDispositionToken + "; filename=\"" + fileName + "\"");

		response.setDateHeader("Expires", new Date().getTime() + cacheAge * 1000);
		response.setHeader("Cache-Control", "max-age=" + cacheAge);

		response.flushBuffer();

		ServletOutputStream out = response.getOutputStream();

		try
		{
			out.write(data, 0, data.length);
		}
		catch (IOException exception)
		{
			exception.printStackTrace();
			response.sendError(HttpServletResponse.SC_PARTIAL_CONTENT);
		}
		finally
		{
			try
			{
				out.flush();
				out.close();
			}
			catch (IOException exception)
			{
				exception.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	static public void returnInlineFile(final HttpServletResponse response, final File file, final String mimeType) throws IOException
	{
		returnFile(response, file, mimeType, ContentDispositionType.INLINE);
	}

	/**
	 *
	 */
	static public String[] rewritedUrlParameters(final HttpServletRequest request)
	{
		String[] result;

		result = longRewritedUrlParameters(request.getRequestURI());

		//
		return (result);
	}

	/**
	 * This method gives a way for a long rewriting URL format. Long as in REST.
	 * 
	 * Sometimes, URL has to be rewrited because we need to put parameter in the
	 * page name.
	 * 
	 * Example:
	 * 
	 * <pre>
	 *  "/good/give_file?id=123&filename=foo.jpg"
	 *  => rewriteShorturl("/good/give_file", "123", "foo.jpg");
	 *  => "/good/give_file-/123/foo.jpg"
	 * </pre>
	 * 
	 * Note: "-/" is used to indicate the start of parameters.
	 * 
	 */
	static public String rewriteLongUrl(final String uri, final String... parameters)
	{
		String result;

		StringList string = new StringList();

		string.append(uri).append("-");
		if ((parameters == null) || (parameters.length == 0))
		{
			string.append("/");
		}
		else
		{
			for (String parameter : parameters)
			{
				string.append("/").append(parameter);
			}
		}

		result = string.toString();

		//
		return (result);
	}

	/**
	 * 
	 * 
	 * @param parameter
	 * @return
	 */
	static String rewriteParameter(final String parameter)
	{
		String result;

		StringBuffer buffer = new StringBuffer(parameter.length());

		char previousCar = NONE;
		for (int charIndex = 0; charIndex < parameter.length(); charIndex++)
		{
			// logger.info("" + charIndex + " " + parameter.charAt(charIndex) +
			// " " + (char) tab[parameter.charAt(charIndex)]);

			char sourceCar = parameter.charAt(charIndex);

			char targetCar;
			if (sourceCar > 255)
			{
				targetCar = '-';
			}
			else
			{
				targetCar = (char) rewritingParameterMapping[sourceCar];
			}

			if (targetCar != NONE)
			{
				if ((targetCar != '-') || ((targetCar == '-') && (previousCar != '-')))
				{
					buffer.append(targetCar);
					previousCar = targetCar;
				}
			}
		}

		if (buffer.charAt(buffer.length() - 1) == '-')
		{
			buffer.setLength(buffer.length() - 1);
		}

		result = buffer.toString();
		logger.info("[" + parameter + "] -> [" + result + "]");
		//
		return (result);
	}

	/**
	 * This method gives a way for a short rewriting URL format.
	 * 
	 * Sometimes, URL has to be rewrited because we need to put parameter in the
	 * page name.
	 * 
	 * Example:
	 * 
	 * <pre>
	 * "/good/article.xhtm?id=123&class=today&title=story's about me"
	 * => rewriteShorturl("/good/article", "xhtml", "123", "Story's aboute me");
	 * => "/good/article-123-today-story-s-about-me.xhtml"
	 * </pre>
	 */
	static public String rewriteShortUrl(final String uri, final String extension, final String... parameters)
	{
		String result;

		StringList string = new StringList();

		string.append(uri);

		for (String parameter : parameters)
		{
			// Not use of String.replaceAll() method in goal to optimize Pattern
			// compile action.
			// string.append("-").append(REWRITE_PARAMETER.matcher(parameter.toLowerCase()).replaceAll("-"));
			string.append("-").append(rewriteParameter(parameter));
		}

		if ((extension != null) && (extension.length() != 0))
		{
			string.append(".").append(extension);
		}

		result = string.toString();

		//
		return (result);
	}

	/**
	 *
	 */
	static public String shortRewritedUrlParameter(final HttpServletRequest request)
	{
		String result;

		result = shortRewritedUrlParameter(request.getRequestURI());

		//
		return (result);
	}

	/**
	 * Return value of the first parameter.
	 */
	static public String shortRewritedUrlParameter(final String path)
	{
		String result;

		String[] results = shortRewritedUrlParameters(path);

		if ((results == null) || (results.length == 0))
		{
			result = null;
		}
		else
		{
			result = results[0];
		}

		//
		return (result);
	}

	/**
	 *
	 */
	static public String[] shortRewritedUrlParameters(final HttpServletRequest request)
	{
		String[] result;

		result = shortRewritedUrlParameters(request.getRequestURI());

		//
		return (result);
	}

	/**
	 * Extract value from a path. Example: "/article-123.xhtml" => "123".
	 */
	static public String[] shortRewritedUrlParameters(final String path)
	{
		String[] result;

		Matcher matcher = SHORT_REWRITED_URL_PARAMETERS.matcher(path);
		if (matcher.matches())
		{
			if (matcher.groupCount() != 1)
			{
				result = null;
			}
			else
			{
				result = matcher.group(1).split("-");
			}
		}
		else
		{
			result = null;
		}

		//
		return (result);
	}
}
