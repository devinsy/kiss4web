/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 */
public class CookieHelper
{
	public enum Scope
	{
		HTTP_AND_HTTPS, HTTPS_ONLY
	}

	static private final Logger logger = LoggerFactory.getLogger(CookieHelper.class);

	/**
	 *
	 */
	static public Cookie buildCookie(final String name, final String value, final int duration)
	{
		Cookie result;

		result = buildCookie(name, value, duration, Scope.HTTP_AND_HTTPS);

		//
		return (result);
	}

	/**
	 * Warning: value is UTF-8 URLEncoded!
	 */
	static public Cookie buildCookie(final String name, final String value, final int duration, final Scope secure)
	{
		Cookie result;

		//
		try
		{
			result = new Cookie(name, java.net.URLEncoder.encode(value, "UTF-8"));
			result.setMaxAge(duration);
			result.setPath("/");

			//
			boolean secureValue;
			if (secure == Scope.HTTPS_ONLY)
			{
				secureValue = true;
			}
			else
			{
				secureValue = false;
			}
			result.setSecure(secureValue);
		}
		catch (UnsupportedEncodingException exception)
		{
			exception.printStackTrace();
			throw new IllegalArgumentException("value is unsupported encoding.");
		}

		//
		return (result);
	}

	/**
	 *
	 */
	static public boolean exists(final HttpServletRequest request, final String key)
	{
		boolean result;

		if (getCookieValue(request, key) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return (result);
	}

	/**
	 *
	 */
	static public Cookie getCookie(final Cookie[] cookies, final String key)
	{
		Cookie result = null;

		if (cookies == null)
		{
			result = null;
		}
		else
		{
			boolean ended = false;
			int cookieCounter = 0;
			while (!ended)
			{
				if (cookieCounter < cookies.length)
				{
					if (key.equals(cookies[cookieCounter].getName()))
					{
						ended = true;
						result = cookies[cookieCounter];
					}
					else
					{
						cookieCounter += 1;
					}
				}
				else
				{
					ended = true;
					result = null;
				}
			}
		}

		//
		return (result);
	}

	/**
	 *
	 */
	static public Cookie getCookie(final HttpServletRequest request, final String key)
	{
		Cookie result = null;

		result = getCookie(request.getCookies(), key);

		//
		return (result);
	}

	/**
	 * Note: value is UTF-8 decoded.
	 */
	static public Object getCookieValue(final Cookie[] cookies, final String key)
	{
		Object result;

		try
		{
			Cookie cookie = getCookie(cookies, key);

			if (cookie == null)
			{
				result = null;
			}
			else
			{
				result = java.net.URLDecoder.decode(cookie.getValue(), "UTF-8");
			}
		}
		catch (UnsupportedEncodingException exception)
		{
			exception.printStackTrace();
			throw new IllegalArgumentException();
		}

		//
		return (result);
	}

	/**
	 *
	 */
	static public Object getCookieValue(final HttpServletRequest request, final String key)
	{
		Object result;

		result = getCookieValue(request.getCookies(), key);

		//
		return (result);
	}

	/**
	 *
	 */
	static public void reset(final HttpServletResponse response, final String key)
	{
		response.addCookie(buildCookie(key, "", 0));
	}

	/**
	 *
	 */
	static public void set(final HttpServletResponse response, final String name, final String value, final int duration)
	{
		response.addCookie(buildCookie(name, value, duration));
	}
}
