/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class ServletDispatcher extends SimpleServletDispatcher
{
	private static final long serialVersionUID = -3471226305721330069L;
	static private Logger logger;

	// protected Servlets servlets;

	/**
	 *
	 */
	@Override
	public void doIt(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.info("==================================================");
		logger.info("getContextPath=[" + request.getContextPath() + "]");
		logger.info("getPathInfo=[" + request.getPathInfo() + "]");
		logger.info("getPathTranslated=[" + request.getPathTranslated() + "]");
		logger.info("getQueryString=[" + request.getQueryString() + "]");
		logger.info("getRequestURI=[" + request.getRequestURI() + "]");
		logger.info("getRequestURL=[" + request.getRequestURL() + "]");
		logger.info("getServletPath=[" + request.getServletPath() + "]");

		String className = pathInfoToClassName(request.getPathInfo());
		logger.info("className=" + className);

		HttpServlet servlet = instanciateServlet(className);

		if (servlet == null)
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			out.println("<html><head></head><body>");
			out.println("Unknow page.");
			out.println("</body></html>");

			out.close();
		}
		else
		{
			servlet.service(request, response);
		}
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
		super.init();
		logger = LoggerFactory.getLogger(this.getClass());
		// this.servlets = new Servlets();
	}
}
