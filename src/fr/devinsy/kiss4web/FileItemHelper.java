/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web;

import java.util.List;

import org.apache.commons.fileupload.FileItem;

/**
 *
 */
public class FileItemHelper
{
	// static private final Logger logger =
	// LoggerFactory.getLogger(FileItemHelper.class);

	/**
	 * List FileItem
	 */
	static public FileItem getItem(final List items, final String name)
	{
		FileItem result;

		if (name == null)
		{
			result = null;
		}
		else
		{
			result = null;
			boolean ended = false;
			int itemIndex = 0;
			while (!ended)
			{
				if (itemIndex < items.size())
				{
					FileItem item = (FileItem) items.get(itemIndex);

					if (name.equals(item.getFieldName()))
					{
						ended = true;
						result = item;
					}
					else
					{
						itemIndex += 1;
					}
				}
				else
				{
					ended = true;
					result = null;
				}
			}
		}

		//
		return (result);
	}

	/**
	 * List FileItem
	 */
	static public String getItemValue(final List items, final String name)
	{
		String result;

		FileItem item = getItem(items, name);

		if (item == null)
		{
			result = null;
		}
		else
		{
			result = item.getString();
		}

		//
		return (result);
	}
}

// ////////////////////////////////////////////////////////////////////////