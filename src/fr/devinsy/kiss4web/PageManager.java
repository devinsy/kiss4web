/**
 * Copyright (C) 2006-2010, 2013-2014 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Kiss4web is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Kiss4web is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Kiss4web.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.kiss4web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.devinsy.kiss4web.security.SecurityAgent;
import fr.devinsy.kiss4web.security.User;

/**
 *
 */
public class PageManager extends HttpServlet
{
	private static final long serialVersionUID = 1983715791417570578L;
	private static PageManager instance = null;
	protected SecurityAgent securityAgent;

	static private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PageManager.class);

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.info("==================================================");
		logger.info("getContextPath=[" + request.getContextPath() + "]");
		logger.info("getPathInfo=[" + request.getPathInfo() + "]");
		logger.info("getPathTranslated=[" + request.getPathTranslated() + "]");
		logger.info("getQueryString=[" + request.getQueryString() + "]");
		logger.info("getRequestURI=[" + request.getRequestURI() + "]");
		logger.info("getRequestURL=[" + request.getRequestURL() + "]");
		logger.info("getServletPath=[" + request.getServletPath() + "]");

		String className = buildClassName(request.getPathInfo());
		logger.info("className=" + className);

		Page page = this.instanciatePage("site." + className);

		if (page == null)
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			out.println("Unknow page.");
		}
		else
		{
			HttpSession session = request.getSession(false);
			String login;
			if (session == null)
			{
				login = null;
			}
			else
			{
				login = (String) session.getAttribute("login");
			}

			if (this.securityAgent.checkPermission(request.getPathInfo(), login))
			{
				page.doIt(request, response);
				logger.info("securityAgent say 'permission OK': (" + login + ", " + request.getPathInfo() + ")");
			}
			else
			{
				logger.info("securityAgent say 'permission KO': (" + login + ", " + request.getPathInfo() + ")");

				if (login == null)
				{
					response.sendRedirect("/gestion/login.xhtml");
				}
				else
				{
					response.setContentType("text/html");
					PrintWriter out = response.getWriter();

					out.println("<html><head></head><body>");
					out.println("Permission denied.");
					out.println("<form method='get' action='javascript:window.back ();'>");
					out.println("    <input type='submit' name='retour' value='Retour' />");
					out.println("</form>");
					out.println("</body></html>");
				}
			}
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
		//
		instance = this;

		try
		{
			this.securityAgent = new SecurityAgent(getInitParameter("securityDataPath"));
		}
		catch (Exception exception)
		{
			throw new ServletException("SecurityAgent initialization failed.", exception);
		}
	}

	/**
	 */
	public Page instanciatePage(final String className)
	{
		Page result;

		Class<Page> pageClass = null;
		try
		{
			pageClass = (Class<Page>) Class.forName(className);
		}
		catch (java.lang.ClassNotFoundException exception)
		{
			result = null;
		}

		logger.info("class=" + pageClass);

		if (pageClass == null)
		{
			result = null;
			logger.error("Unknow page: (" + className + ")");
		}
		else
		{
			try
			{
				result = pageClass.newInstance();
			}
			catch (java.lang.InstantiationException exception)
			{
				logger.error("Can't instanciate page (" + className + ")");
				result = null;
			}
			catch (java.lang.IllegalAccessException exception)
			{
				logger.error("(2) Can't instanciate page (" + className + ")");
				result = null;
			}
		}

		//
		return (result);
	}

	/**
	 *
	 */
	public SecurityAgent securityAgent()
	{
		SecurityAgent result;

		result = this.securityAgent;

		//
		return (result);
	}

	/**
	 *
	 */
	static public String buildClassName(final String pathInfo)
	{
		String result;
		result = null;

		if (pathInfo.equals("/"))
		{
			result = "Accueil";
		}
		else
		{
			String[] tokens = pathInfo.split("/");
			StringBuffer name = new StringBuffer();

			for (int tokenCounter = 1; tokenCounter < tokens.length - 1; tokenCounter++)
			{
				name.append(tokens[tokenCounter]);
				name.append('.');
			}

			if (pathInfo.endsWith("/"))
			{
				name.append(tokens[tokens.length - 1]);
				name.append('.');
			}

			logger.info("==>[" + tokens[tokens.length - 1] + "]");
			name.append(formatClassName(tokens[tokens.length - 1]));

			result = name.toString();
		}

		//
		return (result);
	}

	/**
	 *
	 */
	static public String buildClassName2(final String pathInfo)
	{
		String result;

		if (pathInfo.endsWith(".xhtml"))
		{
			char[] source = pathInfo.toCharArray();

			StringBuffer out = new StringBuffer();
			for (char c : source)
			{
				out.append("[" + c + "]");
			}
			logger.debug(out.toString());

			char[] target = new char[source.length - 7];
			int lastStartToken = 0;
			for (int nChar = 1; nChar < source.length - 5; nChar++)
			{
				char charSource = source[nChar];
				switch (charSource)
				{
					case '/':
						target[nChar - 1] = '.';
						lastStartToken = nChar;
					break;

					case '.':
						target[lastStartToken] = Character.toUpperCase(target[lastStartToken]);
					break;

					default:
						target[nChar - 1] = source[nChar];
				}
			}

			out = new StringBuffer();
			for (char c : target)
			{
				out.append("[" + c + "]");
			}
			logger.debug(out.toString());

			result = new String(target);
		}
		else if (pathInfo.equals("/"))
		{
			result = "Accueil";
		}
		else if (pathInfo.endsWith("/"))
		{
			char[] source = pathInfo.toCharArray();

			StringBuffer out = new StringBuffer();
			for (char c : source)
			{
				out.append("[" + c + "]");
			}
			logger.debug(out.toString());

			char[] target = new char[source.length - 2];
			int lastStartToken = 0;
			for (int nChar = 1; nChar < source.length - 1; nChar++)
			{
				char charSource = source[nChar];
				switch (charSource)
				{
					case '/':
						target[nChar - 1] = '.';
						lastStartToken = nChar + 1;
					break;

					default:
						target[nChar - 1] = source[nChar];
				}
			}

			char[] targetPlus = new char[source.length - lastStartToken];
			targetPlus[0] = '.';
			targetPlus[1] = Character.toUpperCase(source[lastStartToken]);
			int index = 2;
			for (int nChar = lastStartToken + 1; nChar < source.length - 1; nChar++)
			{
				targetPlus[index] = source[nChar];
				index += 1;
			}

			out = new StringBuffer();
			for (char c : target)
			{
				out.append("[" + c + "]");
			}
			logger.debug(out.toString());
			out = new StringBuffer();
			for (char c : targetPlus)
			{
				out.append("[" + c + "]");
			}
			logger.debug(out.toString());

			result = new String(target) + new String(targetPlus);
		}
		else
		{
			logger.debug("unknow case");
			result = null;
		}

		//
		return (result);
	}

	/**
	 *
	 */
	static public String formatClassName(final String name)
	{
		String result;

		result = null;

		String[] splittedLastToken = name.split("\\.");
		String last = splittedLastToken[0];
		// logger.info ("last=" + last);

		String[] tokens = last.split("_");
		StringBuffer all = new StringBuffer();
		for (String token : tokens)
		{
			// logger.info ("tok=" + token);

			all.append(Character.toUpperCase(token.charAt(0)));
			all.append(token.substring(1));
		}

		result = all.toString();

		//
		return (result);
	}

	/**
	 *
	 */
	static public User getUserFromSession(final HttpServletRequest request)
	{
		User result;

		if (request == null)
		{
			result = null;
		}
		else
		{
			HttpSession session = request.getSession(false);

			String login;
			if (session == null)
			{
				result = null;
			}
			else
			{
				login = (String) session.getAttribute("login");
				result = PageManager.instance().securityAgent().users().getByLogin(login);
			}
		}

		//
		return (result);
	}

	/**
	 *
	 */
	public static PageManager instance()
	{
		return instance;
	}
}
